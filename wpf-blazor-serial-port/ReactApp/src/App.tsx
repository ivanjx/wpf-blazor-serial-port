import React, { ChangeEvent, useState } from "react";
import { ISerialPort, SerialPortService } from "./SerialPort";

export const App: React.FC = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [portNames, setPortNames] = useState<string[]>([]);
    const [selectedPortName, setSelectedPortName] = useState<string>("");
    const [serialPort, setSerialPort] = useState<ISerialPort | null>(null);
    const [sendTxt, setSendTxt] = useState("");
    const [receiveTxt, setReceiveTxt] = useState("");

    async function loadPorts() {
        setIsLoading(true);

        try {
            const ports = await SerialPortService.list();
            setPortNames(ports);

            if (ports.length > 0) {
                setSelectedPortName(ports[0]);
            }
        } catch (err: any) {
            alert(err.message);
        } finally {
            setIsLoading(false);
        }
    }

    async function connect() {
        if (selectedPortName == "") {
            alert("Port not selected");
            return;
        }

        try {
            setIsLoading(true);
            const port = await SerialPortService.connect(
                selectedPortName,
                9600,
                5000
            );
            setSerialPort(port);
        } catch (err: any) {
            alert(err.message);
        } finally {
            setIsLoading(false);
        }
    }

    async function disconnect() {
        if (serialPort == null) {
            return;
        }

        serialPort.close();
        setSerialPort(null);
    }

    async function send() {
        if (serialPort == null) {
            alert("Not connected");
            return;
        }

        if (sendTxt == "") {
            alert("Nothing to send");
            return;
        }

        try {
            setIsLoading(true);

            console.log("Sending buff");
            console.log(sendTxt);
            const encoder = new TextEncoder();
            const sendBuff = encoder.encode(
                sendTxt.replaceAll("\n", "\r\n") // Browsers always use '\n' for new line.
            );
            await serialPort.write(sendBuff);

            console.log("Reading response");
            const readBuff = await serialPort.read(10 * 1024);
            const decoder = new TextDecoder();
            const receiveStr = decoder.decode(readBuff);
            setReceiveTxt(receiveStr);
        } catch (err: any) {
            alert(err.message);
        } finally {
            setIsLoading(false);
        }
    }

    function handleSelectedPortNameChange(e: ChangeEvent<HTMLSelectElement>) {
        setSelectedPortName(e.target.value);
    }

    function handleSendChange(e: ChangeEvent<HTMLTextAreaElement>) {
        setSendTxt(e.target.value);
    }

    const Top: React.FC = () => {
        if (serialPort == null) {
            return (
                <div>
                    <button onClick={loadPorts}
                            disabled={isLoading}>
                        Load Ports
                    </button>

                    <div style={{ marginTop: "0.5rem" }}></div>

                    <select value={selectedPortName}
                            onChange={handleSelectedPortNameChange}
                            style={{ minWidth: "5rem" }}
                            disabled={isLoading}>
                        {
                            portNames.map((portName, index) => (
                                <option key={index} value={portName}>
                                    {portName}
                                </option>
                            ))
                        }
                    </select>
                    <button onClick={connect}
                            disabled={isLoading}>
                        Connect
                    </button>
                </div>
            )
        } else {
            return (
                <button onClick={disconnect}>
                    Disconnect
                </button>
            )
        }
    }

    return (
        <div>
            <Top/>

            <div style={{ marginTop: "2rem" }}></div>

            <textarea value={sendTxt}
                      onChange={handleSendChange}
                      placeholder="SEND"
                      disabled={isLoading}
                      style={{ minHeight: "5rem" }}>
            </textarea>

            <div></div>

            <button onClick={send}>
                Send
            </button>

            <div style={{ marginTop: "2rem" }}></div>

            <textarea value={receiveTxt}
                      placeholder="RECEIVE"
                      style={{ minHeight: "5rem" }}
                      readOnly={true}>
            </textarea>
        </div>
    );
}
