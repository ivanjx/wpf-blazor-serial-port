import { App } from "./App";
import { createRoot } from 'react-dom/client';

export function RenderApp() {
    const root = createRoot(document.getElementById('reactapp') as HTMLElement);
    root.render(<App />);
}
