using System;
using System.IO.Ports;
using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace wpf_blazor_serial_port;

public static class SerialPortHelper
{
    [JSInvokable($"{nameof(SerialPortHelper)}.{nameof(ListAsync)}")]
    public static ValueTask<string[]> ListAsync()
    {
        return ValueTask.FromResult(
            SerialPort.GetPortNames());
    }

    [JSInvokable($"{nameof(SerialPortHelper)}.{nameof(ConnectAsync)}")]
    public static async ValueTask<DotNetObjectReference<TsSerialPort>> ConnectAsync(
        string name,
        int baudRate,
        int timeout)
    {
        TsSerialPort port = new TsSerialPort(
            name,
            baudRate,
            timeout);
        await port.ConnectAsync();
        return DotNetObjectReference.Create(port);
    }
}

public class TsSerialPort : IDisposable
{
    SerialPort m_port;
    int m_timeout;

    public TsSerialPort(
        string name,
        int baudRate,
        int timeout)
    {
        m_port = new SerialPort(name, baudRate);
        m_timeout = timeout;
    }

    public ValueTask ConnectAsync()
    {
        if (m_port.IsOpen)
        {
            throw new Exception("Port alread open");
        }
        
        m_port.Open();
        m_port.BaseStream.ReadTimeout = m_timeout;
        m_port.BaseStream.WriteTimeout = m_timeout;
        return ValueTask.CompletedTask;
    }

    [JSInvokable]
    public async ValueTask<byte[]> ReadAsync(int count)
    {
        byte[] buff = new byte[count];
        int read = await m_port.BaseStream.ReadAsync(buff);

        if (read == count)
        {
            return buff;
        }

        byte[] result = new byte[read];
        buff.CopyTo(result.AsSpan());
        return result;
    }

    [JSInvokable]
    public ValueTask WriteAsync(byte[] buff)
    {
        return m_port.BaseStream.WriteAsync(buff);
    }

    [JSInvokable]
    public void Dispose()
    {
        m_port.Dispose();
    }
}
