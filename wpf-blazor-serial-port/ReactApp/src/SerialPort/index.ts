export interface ISerialPortService {
    list(): Promise<string[]>;
    connect(
        name: string,
        baudRate: number,
        timeout: number
    ): Promise<ISerialPort>;
}

class SerialPortServiceImpl implements ISerialPortService {
    async list(): Promise<string[]> {
        // @ts-ignore
        return await DotNet.invokeMethodAsync(
            "wpf_blazor_serial_port",
            "SerialPortHelper.ListAsync"
        );
    }
    
    async connect(name: string, baudRate: number, timeout: number): Promise<ISerialPort> {
        // @ts-ignore
        const handle = await DotNet.invokeMethodAsync(
            "wpf_blazor_serial_port",
            "SerialPortHelper.ConnectAsync",
            name,
            baudRate,
            timeout
        );
        return new SerialPort(handle);
    }
}

export const SerialPortService: ISerialPortService = new SerialPortServiceImpl();

export interface ISerialPort {
    read(count: number): Promise<Uint8Array>;
    write(buff: Uint8Array): Promise<void>;
    close(): void;
}

class SerialPort implements  ISerialPort {
    handle?: any;
    
    constructor(handle: any) {
        this.handle = handle;
    }
    
    async read(count: number): Promise<Uint8Array> {
        return await this.handle.invokeMethodAsync(
            "ReadAsync",
            count
        );
    }
    
    async write(buff: Uint8Array): Promise<void> {
        return await this.handle.invokeMethodAsync(
            "WriteAsync",
            buff
        );
    }
    
    close(): void {
        if (this.handle == null) {
            return;
        }
        
        this.handle.invokeMethodAsync("Dispose");
        this.handle = null;
    }
}
